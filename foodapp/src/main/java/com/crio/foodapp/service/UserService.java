package com.crio.foodapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class UserService {

    public List<String> favoriteFoods(String username) {
        ArrayList<String> foods = new ArrayList<>();
        foods.add("Paneer Paratha");
        foods.add("Biryani");
        foods.add("Margherita Pizza");

        return foods;
    }
}
