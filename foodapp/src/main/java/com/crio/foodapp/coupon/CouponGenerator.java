package com.crio.foodapp.coupon;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

@Component
public class CouponGenerator {

    public String generate() {
        String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        LocalDateTime ldt = LocalDateTime.now();
        int hour = ldt.getHour();
        String letter = String.valueOf(letters.charAt(((int) Math.random()) * letters.length()));
        return letter + hour;
    }

}