package com.crio.foodapp.controller;

import java.util.List;

import com.crio.foodapp.coupon.CouponGenerator;
import com.crio.foodapp.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    CouponGenerator couponGenerator;

    @Autowired
    UserService userService;

    @Value("${user.admin.name}")
    String adminName;

    @GetMapping("/")
	public String index() {
		return couponGenerator.generate();
	}

    @GetMapping("/hello")
    public String hello(@RequestParam String user) {
        String greeting;
        System.out.println(adminName);
        if (user.equalsIgnoreCase(adminName)) {
            greeting = "Welcome, administrator.";
        } else {
            greeting = "Hello, " + user + "!";
        }
        return greeting;
    }

    @GetMapping("/list")
    public String favoriteFoods(@RequestParam String user) {
        List<String> foods = userService.favoriteFoods(user);
        String favoriteFoods = "";
        for (String food : foods) {
            favoriteFoods += food + "<br>";
        }

        return favoriteFoods;
    }

}
